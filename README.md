#CafeX IOS delegate overriding App.

This app is based on a sample app which skins a configured webpage with Live Assist functionality.

To run this App you will need XCode 7.2+ and to do the following:

1.	Open the LiveAssistDemo.xcodeproj file in xCode
2.	Download the relevant FCSDK iOS and Live Assist iOS libraries
3.	Add in these libraries along with any others required in the docs, via Build Phases >> Link Binary with Libraries
4.	Update the Search Paths in Build Settings to point to the relevant relative paths to the libraries you have downloaded locally.

The purpose of the App is to demonstrate how to implement the Live Assist IOS delegate protocols in order to change the application behaviour or to provide language support to override the default pop-up and banner functionality.

To override the default banner a BannerView has been added to the application, which is added to the ViewController and the banner text is set and removed from the AssistSDKDelegate:cobrowseActiveDidChangeTo method. Search for 'banner' to find all code changes needed to implement this change.

The default pop-ups are implemented in 

1. AssistSDKDelegate : assistSDKDidEncounterError
2. ASDKScreenShareRequestedDelegate : assistSDKScreenShareRequested
3. ASDKPushAuthorizationDelegate : displaySharedDocumentRequested

The localizations have been done for Spanish, Japanese and Turkish. Based on the tutorial at http://www.raywenderlich.com/64401/internationalization-tutorial-for-ios-2014
Please note: The translations were done using google translate so apologises for any linguistic mistakes.

The AssistSDKAnnotationDelegate and AssistSDKDocumentDelegate methods have also been implemented and just trigger an English language popup when they are called.

You will need to add the Live Assist and FCSDK libraries as the ones declared are not included in the zip file.

