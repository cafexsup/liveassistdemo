//
//  LiveAssistDemoTests.m
//  LiveAssistDemoTests
//
//  Created by Kevin Glass on 16/01/2014.
//  Copyright (c) 2014 CafeX. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface LiveAssistDemoTests : XCTestCase

@end

@implementation LiveAssistDemoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
