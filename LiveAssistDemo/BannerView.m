//
//  BannerView.m
//  AssistSample
//
//  Created by Robert Doyle on 11/12/2015.
//
//

#import "BannerView.h"

@implementation BannerView {
    CATextLayer *textLayer;
}

- (id) initWithFrame:(CGRect) frame {
    
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (void) setBannerText:(NSString *)text {
    textLayer = [[CATextLayer alloc] init];
    
    // @TODO Fill in colour
    textLayer.string = text;
    textLayer.frame = self.frame;
    UIColor *bgColour = [UIColor colorWithRed:0.333f green:0.42f blue:0.184f alpha:0.8];
    
    UIColor *fgColour = [UIColor colorWithRed:0.961f green:0.961f blue:0.961f alpha:1.0];
    textLayer.foregroundColor = [fgColour CGColor];
    textLayer.backgroundColor = [bgColour CGColor];
    textLayer.fontSize = 12;
    textLayer.name = @"banner";
    textLayer.alignmentMode = kCAAlignmentCenter;
    
    [self.layer addSublayer:textLayer];
}

- (void) removeBannerText {
    [textLayer removeFromSuperlayer];
}

@end
