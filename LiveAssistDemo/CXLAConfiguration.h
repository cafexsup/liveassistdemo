/*
 * Copyright (c) 2013-2014 CafeX Communications and other contributors,
 * http://www.cafex.com
 
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

//
//  CXLAConfiguration.h
//  applestore
//
//  Created by Gilles Compienne on 10/02/2014.
//

#import <Foundation/Foundation.h>

#define CXLA_DEFAULT_ICON_IMAGE @"black"

@interface CXLAConfiguration : NSObject

+ (void)refreshConfig;

+(NSString *) getServerHost;
+(NSString *) getWebsiteAddress;
+(NSString *) getIconImage;
+(NSString *) getTargettedAgent;
+(BOOL) getAutostart;
+(NSString *) getUsername;

@end
