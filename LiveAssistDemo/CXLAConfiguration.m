//
/*
 * Copyright (c) 2013-2014 CafeX Communications and other contributors,
 * http://www.cafex.com
 
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

//  CXLAConfiguration.m
//  applestore
//
//  Created by Gilles Compienne on 10/02/2014.
//

#import "CXLAConfiguration.h"

static NSUserDefaults *systemSettings;
static NSString* const DEFAULT_SERVER_NAME = @"192.168.9.28";

@implementation CXLAConfiguration

+(void)initialize {
    NSLog(@"Initializing the config change listener.");
    systemSettings = [NSUserDefaults standardUserDefaults];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(defaultsChanged:)
                   name:NSUserDefaultsDidChangeNotification
                 object:nil];
}

// Listens for system settings configuration changes
+ (void)defaultsChanged:(NSNotification *)notification {
    // Get the user defaults
    systemSettings = (NSUserDefaults *)[notification object];
    
    // Do something with it
    NSLog(@"Detected configuration change.");
    
}

+ (void)refreshConfig {
    [systemSettings synchronize];
}

+(NSString *) getServerHost {
    NSString *serverAdr = [systemSettings stringForKey:@"serverAddress"];
    if (!serverAdr) {
        serverAdr = DEFAULT_SERVER_NAME;
    }
    return serverAdr;
}


+(NSString *) getWebsiteAddress {
    NSString *address = [systemSettings stringForKey:@"websiteAddress"];
    if ((address == nil) || ([address length] == 0)) {
        address = @"http://www.cafex.com";
        
    }
    return address;
}


+(NSString *) getIconImage {
    NSString *imageName = [systemSettings stringForKey:@"iconImage"];
    if ((imageName == nil) || ([imageName length] == 0)) {
        imageName = CXLA_DEFAULT_ICON_IMAGE;
    }
    return imageName;
}

+(NSString *) getTargettedAgent {
    NSString *targettedAgent = [systemSettings stringForKey:@"targettedAgent"];
    if ((targettedAgent == nil) || ([targettedAgent length] == 0)) {
        targettedAgent = @"agent1";
    }
    return targettedAgent;
}

+(BOOL) getAutostart {
    id autostart = [systemSettings valueForKey:@"autostart"];
    if (autostart == nil) {
        return FALSE;
    } else {
        return [autostart boolValue];
    }
}

+(NSString *) getUsername {
    NSString *username = [systemSettings stringForKey:@"username"];
    if ([username length] == 0) {
        username = nil;
    }
    return username;
}

@end
