/*
 * Copyright (c) 2013-2016 CafeX Communications and other contributors,
 * http://www.cafex.com
 
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

//
//  ViewController.m
//  LiveAssistDemo
//
//  Created by Kevin Glass on 16/01/2014.
//

#import "ViewController.h"
#import "AssistSDK.h"
#import "CXLAConfiguration.h"
#import "BannerView.h"

@interface ViewController ()

@property IBOutlet UIWebView *webview;
@property (nonatomic, retain) IBOutlet UIImageView *backButton;
@property (nonatomic, retain) IBOutlet UIImageView *forwardButton;
@property (nonatomic, retain) IBOutlet UIImageView *refreshButton;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)forwardButtonPressed:(id)sender;
- (IBAction)refreshButtonPressed:(id)sender;

// Implementing our own custom banner view
@property BannerView *bannerView;

// Properties for ASDKScreenShareRequestedDelegate and AssistSDKDocumentDelegate implementations
@property IBOutlet UIAlertView *screenSharingAlertView;
@property (nonatomic, copy) void (^allowScreenShareHandler)();
@property (nonatomic, copy) void (^denyScreenShareHandler)();

@property IBOutlet UIAlertView *sharedDocumentAlertView;
@property (nonatomic, copy) void (^allowSharedDocumentHandler)();
@property (nonatomic, copy) void (^denySharedDocumentHandler)();

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //added to get image name from defaults
    NSString *imageName = [CXLAConfiguration getIconImage];
    
    NSString *address = [CXLAConfiguration getWebsiteAddress];
    
    NSString *useIcon = [NSString stringWithFormat:@"%@.png",imageName];
    
    UIImage *iconImage = [UIImage imageNamed:useIcon];
    
    if (iconImage == nil) {
        //Image file not found
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Icon Image Not Found" message:@"The Assist icon image configured in Settings was not found.  Using default icon image." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        imageName = CXLA_DEFAULT_ICON_IMAGE;
        useIcon = [NSString stringWithFormat:@"%@.png", imageName];
        iconImage = [UIImage imageNamed:useIcon];
    }

    NSLog(@"View Did load in Tab Controller");
    UIButton *button = [UIButton buttonWithType: UIButtonTypeCustom];
    [button setImage: iconImage forState: UIControlStateNormal];
    [button setImage: iconImage forState: UIControlStateHighlighted];
    [button addTarget:self action:@selector(startLiveAssist:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame: CGRectMake(10, 10, 100, 100)];
    //Added to make button draggable
    button.userInteractionEnabled = YES;
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc]
                                        initWithTarget:self
                                        action:@selector(buttonDragged:)];
	[button addGestureRecognizer:gesture];
    //end added to make button draggable
    [self.view addSubview: button];
    
    // Add Banner view to the controller's view. Set it just below the status bar
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGRect bannerFrame = CGRectMake(0,10, screenBounds.size.width, 18);
    self.bannerView = [[BannerView alloc] initWithFrame:bannerFrame];
    [self.view addSubview:self.bannerView];
    
    NSURL* nsUrl = [NSURL URLWithString: address];
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [self.webview loadRequest:request];
}


//button draggable code
- (void)buttonDragged:(UIPanGestureRecognizer *)gesture
{
	UILabel *button = (UILabel *)gesture.view;
	CGPoint translation = [gesture translationInView:button];
    
	// move label
	button.center = CGPointMake(button.center.x + translation.x,
                               button.center.y + translation.y);
    
	// reset translation
	[gesture setTranslation:CGPointZero inView:button];
}


- (IBAction) startLiveAssist:(id)sender {
    NSString *server = [CXLAConfiguration getServerHost];
    
    NSString *username = [CXLAConfiguration getUsername];
    
    NSLog(@"Starting LiveAssist with server %@", server);
    NSMutableDictionary *config = [NSMutableDictionary
                                   dictionaryWithDictionary:@{@"destination" : [CXLAConfiguration getTargettedAgent],
                                                              @"acceptSelfSignedCerts" : @YES}];
    
    if (username != nil) {
        config[@"username"] = username;
    }
    
    // For screen share request, add a Screen share request delegate to config
    // Similarly, for document push authorisation:
    config[@"pushDelegate"] = self;
    config[@"screenShareRequestedDelegate"] = self;
    
    //The AssistSDKDelegate is not added to the config like the above delegates but is added by calling AssistSDK:addDelegate
    [AssistSDK addDelegate:self];
    
    [AssistSDK startSupport:server supportParameters:config];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(id)sender {
    if ([self.webview canGoBack]) {
        [self.webview goBack];
    }
}

- (IBAction)forwardButtonPressed:(id)sender {
    if ([self.webview canGoForward]) {
        [self.webview goForward];
    }
}

- (IBAction)refreshButtonPressed:(id)sender {
    [self.webview reload];
}

- (void)alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex {
   if (self.screenSharingAlertView == alert) {
        if (buttonIndex == 0) {
            self.denyScreenShareHandler();
        } else {
            self.allowScreenShareHandler();
        }
    }
    else if (self.sharedDocumentAlertView == alert) {
        if (buttonIndex == 0) {
            self.denySharedDocumentHandler();
        } else {
            self.allowSharedDocumentHandler();
        }
    }
    else {
        NSLog(@"Alert triggered that was for neither screen or document sharing");
    }
}

- (void) assistSDKScreenShareRequested:(void (^)(void))allow deny:(void (^)(void))deny {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Co-browsing", nil)
                                                    message:NSLocalizedString(@"Would you like to enable co-browsing?", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    
    // Store screenSharingAlertView so we know which alert is calling the UIAlertViewDelegate callback
    self.screenSharingAlertView = alert;
    
    // store the deny/allow callbacks for use in UIAlertViewDelegate clickedButtonAtIndex callback when the use clicks Cancel or OK
    self.denyScreenShareHandler = deny;
    self.allowScreenShareHandler = allow;
    [alert show];
}

- (void)displaySharedDocumentRequested:(ASDKSharedDocument *)sharedDocument allow: (void (^)(void)) allow deny: (void (^)(void)) deny {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Shared Document", nil)
                                                    message:NSLocalizedString(@"The agent wishes to send you a shared document. Would you like to view it?", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    
    // Store sharedDocumentAlertView so we know which alert is calling the UIAlertViewDelegate callback
    self.sharedDocumentAlertView = alert;
    
    // store the deny/allow callbacks for use in UIAlertViewDelegate clickedButtonAtIndex callback when the use clicks Cancel or OK
    self.denySharedDocumentHandler = deny;
    self.allowSharedDocumentHandler = allow;
    [alert show];
}



#pragma mark AssistSDKAnnotationDelegate protocols
- (void) assistSDKWillAddAnnotation:(NSNotification*)notification
{
    [self showAlertWithProtocol:@"assistSDKWillAddAnnotation" event:@"WillAddAnnotation"];
}
- (void) assistSDKDidAddAnnotation:(NSNotification*)notification
{
    [self showAlertWithProtocol:@"assistSDKDidAddAnnotation" event:@"DidAddAnnotation"];
}
- (void) assistSDKDidClearAnnotations:(NSNotification*)notification
{
    [self showAlertWithProtocol:@"assistSDKDidClearAnnotations" event:@"DidClearAnnotations"];
}

#pragma mark AssistSDKDocumentDelegate protocols
- (void) assistSDKDidOpenDocument:(NSNotification*)notification
{
    //ASDKSharedDocument *sharedDoc =(ASDKSharedDocument*) notification.object;
    //[sharedDoc close];
    [self showAlertWithProtocol:@"assistSDKDidOpenDocument" event:@"DidOpenDocument"];
}
- (void) assistSDKUnableToOpenDocument:(NSNotification*)notification
{
    [self showAlertWithProtocol:@"assistSDKUnableToOpenDocument" event:@"UnableToOpenDocument"];
}
- (void) assistSDKDidCloseDocument:(NSNotification*)notification
{
    [self showAlertWithProtocol:@"assistSDKDidCloseDocument" event:@"DidCloseDocument"];
}


#pragma mark AssistSDKDelegate protocol
- (void) assistSDKDidEncounterError:(NSNotification*)notification
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Encountered An Error", nil)
                                                    message:NSLocalizedString(@"The application encountered an error.", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                          otherButtonTitles:nil];
    [alert show];
}
- (void) cobrowseActiveDidChangeTo:(BOOL)active
{
    //Override the default banner
    if (active) {
        [self.bannerView setBannerText:NSLocalizedString(@"Your application is currently being shared with an agent", nil)];
    } else {
        [self.bannerView removeBannerText];
    }
}


// Helper method
-(void)showAlertWithProtocol:(NSString*)protocol event:(NSString*)event
{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:protocol
                                                       message:[NSString stringWithFormat:@"implement the %@ delegate method to provide custom behaviour when %@ happens" , protocol, event]
                                                      delegate:self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
}

@end
