/*
 * Copyright (c) 2013-2014 CafeX Communications and other contributors,
 * http://www.cafex.com
 
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

//
//  AppDelegate.m
//  LiveAssistDemo
//
//  Created by Kevin Glass on 16/01/2014.
//

#import "AppDelegate.h"
#import "AssistSDK.h"
#import "CXLAConfiguration.h"

#define URL_PREFIX @"la"

#define URLS_PREFIX @"las"

@interface AppDelegate ()
{
    BOOL loadURLRequested;
    NSString *protocol;
    NSString *server;
    NSNumber *port;
    NSString *agent;
    NSString *correlationID;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSURL *requestedURL = [launchOptions objectForKey:@"UIApplicationLaunchOptionsURLKey"];
    // By default we will usually accept the app load request...
    BOOL result = YES;
    
    loadURLRequested = false;
    if (requestedURL != nil) {
        NSString *scheme = [requestedURL scheme];
        if ([scheme caseInsensitiveCompare:URL_PREFIX] == NSOrderedSame) {
            NSLog(@"Application launched in order to handle URL request.");
            loadURLRequested = TRUE;
        } else if ([scheme caseInsensitiveCompare:URLS_PREFIX] == NSOrderedSame) {
            NSLog(@"Application launched in order to handle URL request (secured).");
            loadURLRequested = TRUE;
        } else {
            NSLog(@"Requested URL (%@) is not for LiveAssist Generic Demo to handle.", requestedURL);
            result = NO;
        }
    }
    return result;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    loadURLRequested = FALSE;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // First need to force a config refresh as update events will reach us only after applicationDidBecomeActive ends.
    [CXLAConfiguration refreshConfig];
    if (loadURLRequested) {
        NSString *url = [[NSString alloc] initWithFormat:@"%@://%@:%@/", protocol, server, port];
        if (agent != nil) {
            NSLog(@"Starting LiveAssist as requested by URL trigger. Going to %@ for agent %@", url, agent);
            NSDictionary *config = @{@"destination" : agent,
                                     @"acceptSelfSignedCerts" : @YES};
            [AssistSDK startSupport:url supportParameters:config];
        } else {
            NSLog(@"Starting LiveAssist as requested by URL trigger.  Going to %@ for correlationId %@", url, correlationID);
            NSDictionary *config = @{@"correlationId" : correlationID,
                                     @"acceptSelfSignedCerts" : @YES};
            [AssistSDK startSupport:url supportParameters:config];
        }
    } else if ([CXLAConfiguration getAutostart]) {
        server = [CXLAConfiguration getServerHost];
        agent = [CXLAConfiguration getTargettedAgent];
        NSLog(@"Starting LiveAssist as requested by autostart from server %@ with agent %@", server, agent);
        NSDictionary *config = @{@"destination" : agent,
                                 @"acceptSelfSignedCerts" : @YES};
        [AssistSDK startSupport:server supportParameters:config];
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if (url != nil) {
        NSString *scheme = [url scheme];
        if (([scheme caseInsensitiveCompare:URL_PREFIX] == NSOrderedSame) || ([scheme caseInsensitiveCompare:URLS_PREFIX] == NSOrderedSame)) {
            NSLog(@"Requested to load URL: %@", url);
            loadURLRequested = TRUE;
            if ([scheme caseInsensitiveCompare:URL_PREFIX] == NSOrderedSame) {
                protocol = @"http";
            } else {
                protocol = @"https";
            }
            server = [url host];
            port = [url port];
            if (port == nil) {
                if ([protocol isEqualToString:@"http"]) {
                    port = [NSNumber numberWithInt:8080];
                } else {
                    port = [NSNumber numberWithInt:8443];
                }
            }
            NSString *query = [url query];
            NSLog(@"query = %@", query);
            if ([query hasPrefix:@"agent="]) {
                agent = [query substringFromIndex:6];
            } else if ([query hasPrefix:@"correlationId="]) {
                correlationID = [query substringFromIndex:14];
                NSLog(@"correlationID = %@", correlationID);
            }
            if ([agent length] == 0 && correlationID == nil) {
                agent = @"agent1";
            }
        } else {
            NSLog(@"Requested URL (%@) is not for LiveAssist Generic Demo to handle.", url);
            loadURLRequested = FALSE;
            return NO;
        }
    }
    return YES;
}

@end
