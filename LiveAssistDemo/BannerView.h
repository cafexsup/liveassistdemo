//
//  BannerView.h
//  AssistSample
//
//  Created by Robert Doyle on 11/12/2015.
//
//

#import <UIKit/UIKit.h>

@interface BannerView : UIView

- (id) initWithFrame:(CGRect) frame;
- (void) setBannerText:(NSString *)text;
- (void) removeBannerText;
@end
